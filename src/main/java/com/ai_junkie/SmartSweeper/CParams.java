package com.ai_junkie.SmartSweeper;

public class CParams {

	static double dPi                 = 3.14159265358979;
	static double dHalfPi             = dPi / 2;
	static double dTwoPi              = dPi * 2;
	static int iNumOutputs            = 2;
	static double dMaxTurnRate        = 0.3;
	static double dMaxSpeed           = 1;
	static int iSweeperScale          = 5;

//	static int WindowWidth            = 1200;
//	static int WindowHeight           = 900;
//	static int iNumSweepers           = 300;
//	static int iNumMines              = 2500;
	
	static int iNumSweepers           = 10;
	static int iNumMines              = 50;
	static int WindowWidth            = 600;
	static int WindowHeight           = 480;
	
	static int iNumTicks              = 150000;
	static double dMineScale          = 2;

}

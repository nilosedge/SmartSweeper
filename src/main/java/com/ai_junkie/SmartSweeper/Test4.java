package com.ai_junkie.SmartSweeper;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

public class Test4 extends JFrame {

	public static void main(String[] args) {
		new Test4();

	}
	
	public Test4() {
		super("Graph");
		
		setTitle("Points");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		int w = 200;
		int h = 200;
		setSize(w, h);
		setLocationRelativeTo(null);
		//setUndecorated(true);
		setVisible(true);
		createBufferStrategy(2);
		
		while(true) {
			BufferStrategy bf = getBufferStrategy();
	    	Graphics2D g = (Graphics2D)bf.getDrawGraphics();

	    	
	    	
	    	double r = Math.random() * 8;
	    	
	    	double inx1 = -Math.sin(r);
	    	double iny1 = Math.cos(r);
	    	
	    	double r2 = Math.random() * 8;
	    	
	    	double inx2 = -Math.sin(r2);
	    	double iny2 = Math.cos(r2);
	    	
	    	int x1 = (int)(inx1 * (w/4));
	    	int y1 = (int)(iny1 * (h/4));
	    	
	    	int x2 = (int)(inx2 * (w/4));
	    	int y2 = (int)(iny2 * (h/4));
	    	
	    	//System.out.println("X: " + x + " Y: " + y);
	    	
	    	g.setBackground(Color.WHITE);
	    	g.clearRect(0, 0, 200, 200);
	    	g.setColor(Color.BLUE);
	    	g.drawLine((w/2), (h/2), x1+(w/2), y1+(h/2));
	    	
	    	g.setColor(Color.RED);
	    	g.drawLine((w/2), (h/2), x2+(w/2), y2+(h/2));


	    	g.dispose();
	    	bf.show();
	    	try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    	
	    	
		}
	}

}

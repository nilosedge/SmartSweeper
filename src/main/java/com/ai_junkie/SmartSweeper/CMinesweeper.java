package com.ai_junkie.SmartSweeper;

import java.util.ArrayList;

import com.ai_junkie.SmartSweeper.nodes.NeuralNet;
import com.ai_junkie.SmartSweeper.printing.AINode;
import com.ai_junkie.SmartSweeper.printing.NodeVisitor;

public class CMinesweeper implements AINode {
	//the minesweeper's neural net

	int[] top = {4, 6, 6, 2};
	
	private boolean disableNNUpdates = false;
	
	public NeuralNet m_ItsBrain = new NeuralNet(top);

	//its position in the world
	SVector2D m_vPosition;

	//direction sweeper is facing
	SVector2D m_vLookAt;

	//its rotation (surprise surprise)
	double m_dRotation;

	double m_dSpeed;

	//to store output from the ANN
	double m_lTrack;
	double m_rTrack;

	int incFit = 0;

	//index position of closest mine
	int m_iClosestMine;
	
	private DirectionFrame direction;
		
	public CMinesweeper(boolean disableNNUpdates) {
		m_dRotation = Utils.RandDouble()*CParams.dTwoPi;
		m_lTrack = CParams.dMaxSpeed / 2;
		m_rTrack = CParams.dMaxSpeed / 2;
		m_iClosestMine = 0;
		//System.out.println(disableNNUpdates);
		this.disableNNUpdates = disableNNUpdates;

		//create a random start position
		m_vPosition = new SVector2D((Utils.RandDouble() * CParams.WindowWidth), (Utils.RandDouble() * CParams.WindowHeight));
		m_vLookAt = new SVector2D(0, 0);
		
		//direction = new DirectionFrame("Direction Frame", 200, 200);
		//new Thread(direction).start();
	}

//	public void Reset() {
//		//reset the sweepers positions
//		m_vPosition = new SVector2D((Utils.RandDouble() * CParams.WindowWidth), (Utils.RandDouble() * CParams.WindowHeight));
//			
//									
//		//and the rotation		
//		m_dRotation = Utils.RandDouble()*CParams.dTwoPi;
//													
//	}		
		
		
	public void WorldTransform(ArrayList<SPoint> sweeper) {
		//create the world transformation matrix
		C2DMatrix matTransform = new C2DMatrix();

		matTransform.Scale(CParams.iSweeperScale, CParams.iSweeperScale);

		matTransform.Rotate(m_dRotation);

		matTransform.Translate(m_vPosition.getX(), m_vPosition.getY());

		matTransform.TransformSPoints(sweeper);
	}
		
	public boolean Update(ArrayList<SVector2D> mines) {
		
		//this will store all the inputs for the NN
		ArrayList<Double> inputs = new ArrayList<Double>();

		//get vector to closest mine
		SVector2D vClosestMine = GetClosestMine(mines);

		//normalise it
		Utils.Vec2DNormalize(vClosestMine);

		//add in vector to closest mine
		inputs.add(vClosestMine.getX());
		inputs.add(vClosestMine.getY());

		//add in sweepers look at vector
		inputs.add(m_vLookAt.getX());
		inputs.add(m_vLookAt.getY());

		//update the brain and get feedback
		m_ItsBrain.feedForward(inputs);
		
		ArrayList<Double> output = m_ItsBrain.getResults();

		//make sure there were no errors in calculating the 
		//output
//		if (output.size() < CParams.iNumOutputs) {
//			return false;
//		}

		//assign the outputs to the sweepers left & right tracks
		m_lTrack = output.get(0);
		m_rTrack = output.get(1);
		//System.out.println("NN Suggests: Left: " + output.get(0) + " Right: " + output.get(1));

		
		//calculate steering forces
		double RotForce = Utils.Clamp(m_lTrack - m_rTrack, -CParams.dMaxTurnRate, CParams.dMaxTurnRate);
//		System.out.println("Left: " + m_lTrack);
//		System.out.println("Right: " + m_rTrack);
		//clamp rotation
		

		// Back prop what values should be
		double closestMine = Math.atan2(vClosestMine.y, vClosestMine.x);
		double looking = Math.atan2(Math.cos(m_dRotation), -Math.sin(m_dRotation));
		double diff = Math.acos((vClosestMine.x * -Math.sin(m_dRotation)) + (vClosestMine.y * Math.cos(m_dRotation)));
		
//		System.out.println("Closest: " + closestMine);
//		System.out.println("Looking: " + looking);
//		System.out.println("Diff: " + diff);
		
		ArrayList<Double> targetValues = new ArrayList<Double>();
		if(closeTo(transPoint(looking - diff), closestMine)) {
			//System.out.println("Turning Left: ");
			if(diff > CParams.dMaxTurnRate) {
				targetValues.add((CParams.dMaxSpeed / 2) - (CParams.dMaxTurnRate / 2));
				targetValues.add((CParams.dMaxSpeed / 2) + (CParams.dMaxTurnRate / 2));
			} else {
				targetValues.add((CParams.dMaxSpeed / 2) - (diff / 2));
				targetValues.add((CParams.dMaxSpeed / 2) + (diff / 2));
			}
		}
		if(closeTo(transPoint(looking + diff), closestMine)) {
			//System.out.println("Turning Right: ");
			if(diff > CParams.dMaxTurnRate) {
				targetValues.add((CParams.dMaxSpeed / 2) + (CParams.dMaxTurnRate / 2));
				targetValues.add((CParams.dMaxSpeed / 2) - (CParams.dMaxTurnRate / 2));
			} else {
				targetValues.add((CParams.dMaxSpeed / 2) + (diff / 2));
				targetValues.add((CParams.dMaxSpeed / 2) - (diff / 2));
			}
		}
		//System.out.println("Target: " + targetValues);
		m_ItsBrain.backPropagate(targetValues);

		
		
		m_dRotation += RotForce;

		//m_dSpeed = Utils.Clamp(m_lTrack + m_rTrack, -CParams.dMaxSpeed, CParams.dMaxSpeed);
		m_dSpeed = m_lTrack + m_rTrack;

		//update Look At 
		m_vLookAt.setX(-Math.sin(m_dRotation));
		m_vLookAt.setY(Math.cos(m_dRotation));
		
		//update position
		m_vPosition = m_vPosition.add(m_vLookAt.multiply(m_dSpeed));
		//wrap around window limits
		if (m_vPosition.getX() > CParams.WindowWidth) m_vPosition.setX(0);
		if (m_vPosition.getX() < 0) m_vPosition.setX(CParams.WindowWidth);
		if (m_vPosition.getY() > CParams.WindowHeight) m_vPosition.setY(0);
		if (m_vPosition.getY() < 0) m_vPosition.setY(CParams.WindowHeight);


		

		//direction.update(m_vLookAt, vClosestMine, Math.cos(m_dRotation));
		
		
		return true;
	}
	
//	public void Reward() {
//		ArrayList<Double> targetValues = new ArrayList<Double>();
//		targetValues.add(m_lTrack);
//		targetValues.add(m_rTrack);
//		m_ItsBrain.backPropigation(targetValues);
//	}
	
	public double transPoint(double a) {
		if(a > Math.PI) {
			a -= (2 * Math.PI);
		}
		if(a < -Math.PI) {
			a += (2 * Math.PI);
		}
		return a;
	}
	
	public boolean closeTo(double a, double b) {
		double d = (a - b);
		return (-0.000001 < d && d < 0.000001);
	}
		
	public SVector2D GetClosestMine(ArrayList<SVector2D> mines) {
		double closest_so_far = 99999;

		SVector2D vClosestObject = new SVector2D(0, 0);

		//cycle through mines to find closest
		for (int i = 0; i < mines.size(); i++) {
			double len_to_object = Utils.Vec2DLength(mines.get(i).minus(m_vPosition));

			if (len_to_object < closest_so_far) {
				closest_so_far = len_to_object;
				//vClosestObject = m_vPosition.minus(mines.get(i));
				vClosestObject = mines.get(i).minus(m_vPosition);
				m_iClosestMine = i;
			}
		}

		return vClosestObject;
	}
		
		
	public int CheckForMine(ArrayList<SVector2D> mines, double size) {
		SVector2D DistToObject = m_vPosition.minus(mines.get(m_iClosestMine));

		if (Utils.Vec2DLength(DistToObject) < (size + 5)) {
			return m_iClosestMine;
		}
		return -1;
	}
		
	public SVector2D Position() {
		return m_vPosition;
	}
	
	public void incFit() {
		incFit++;
	}
	
	public int getIncFit() {
		return incFit;
	}
	public void setIncFit(int incFit) {
		this.incFit = incFit;
	}
	
	public void updateBrain(CMinesweeper betterBrain) {
		m_dRotation = betterBrain.m_dRotation;
		m_dSpeed = betterBrain.m_dSpeed;
		m_lTrack = betterBrain.m_lTrack;
		m_rTrack = betterBrain.m_rTrack;
		m_ItsBrain = new NeuralNet(top);
		m_ItsBrain.copyWeights(betterBrain.m_ItsBrain);
	}

	public void Accept(NodeVisitor visitor) {
		visitor.Visit(this);
	}

}

package com.ai_junkie.SmartSweeper.nodes;

import java.util.ArrayList;

import com.ai_junkie.SmartSweeper.printing.AINode;
import com.ai_junkie.SmartSweeper.printing.NodeVisitor;

public class Neuron implements AINode {

	public String name = "";
	private double outputValue = 1.0;

	private ArrayList<NeuronConnection> inputs = new ArrayList<NeuronConnection>();
	private ArrayList<NeuronConnection> outputs = new ArrayList<NeuronConnection>();

	public void feedForward(Double outputValue) {
		this.outputValue = outputValue;
		for(NeuronConnection nc: outputs) {
			nc.setValue(this.outputValue);
		}
	}
	
	public void feedForward() {
		feedForward(transFunc(sumWeights()));
	}
	
	public double sumWeights() {
		double sum = 0.0;
		for(NeuronConnection nc: inputs) {
			sum += (nc.getValue() * nc.getWeight());
		}
		return sum;
	}
	
	public double sumDeltas() {
		double sum = 0.0;
		for(NeuronConnection nc: outputs) {
			sum += (nc.getDelta() * nc.getWeight());
		}
		return sum;
	}
	
	public void calcDelta(Double target) {
		
		double delta = (target - outputValue) * transFuncDer(sumWeights());
		for(NeuronConnection nc: inputs) {
			nc.setDelta(delta);
		}
		//System.out.println("OL: " + name + " Sum: " + sumWeights() + " Output: + " + outputValue + " Delta: " + delta);
//		setThresholdDiff((NeuralNet.learningRate * delta * threshold) + (NeuralNet.momentum * thresholdDiff));
//		for(NeuronConnection nc: inputs) {
//			nc.setGradient((NeuralNet.learningRate * delta * outputValue) + (NeuralNet.momentum * nc.getGradient()));
//		}
	}

	public void calcDelta() {
//		if(inputs.size() > 0) {
//			setDelta(sumDeltas() * transFuncDer(sumWeights()));
//		} else if(outputs.size() == 1) {
//			setDelta(0.0);
//		} else {
//			setDelta(sumDeltas());
//		}

		double delta = sumDeltas() * transFuncDer(sumWeights());
		for(NeuronConnection nc: inputs) {
			nc.setDelta(delta);
		}
		
		//System.out.println("HL: " + name + " Sum: " + sumWeights() + " Output: + " + outputValue + " Delta: " + delta);
//		setThresholdDiff((NeuralNet.learningRate * delta * threshold) + (NeuralNet.momentum * thresholdDiff));
//		for(NeuronConnection nc: inputs) {
//			nc.setGradient((NeuralNet.learningRate * delta * outputValue) + (NeuralNet.momentum * nc.getGradient()));
//		}
	}
	

	public void calcGradients() {
		for(NeuronConnection nc: inputs) {
			nc.setGradient(nc.getDelta() * nc.getValue());
			nc.setWeight(nc.getWeight() + nc.getGradient());
		}
	}

	private double transFuncDer(double x) {
		double trans = transFunc(x);
		double ret = trans * (1 - trans);
		return ret;
		
		//return ((1 - Math.tanh(x)) * (1 + Math.tanh(x)));
	}

	// Also known as activation function
	// Also known as transfer function
	private double transFunc(double x) {
		if (x < -45.0) return 0.0;
		else if (x > 45.0) return 1.0;
		return 1/(1+Math.exp(-x));
		
		//return Math.tanh(x);
	}
	
	public double getOutputValue() {
		return outputValue;
	}
	public void setOutputValue(double outputValue) {
		this.outputValue = outputValue;
	}
	public ArrayList<NeuronConnection> getInputs() {
		return inputs;
	}
	public void setInputs(ArrayList<NeuronConnection> inputs) {
		this.inputs = inputs;
	}
	public ArrayList<NeuronConnection> getOutputs() {
		return outputs;
	}
	public void setOutputs(ArrayList<NeuronConnection> outputs) {
		this.outputs = outputs;
	}
	
	public void Accept(NodeVisitor visitor) {
		visitor.Visit(this);
	}
}

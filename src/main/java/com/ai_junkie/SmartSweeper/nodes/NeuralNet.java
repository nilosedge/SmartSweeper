package com.ai_junkie.SmartSweeper.nodes;

import java.util.ArrayList;

import com.ai_junkie.SmartSweeper.printing.AINode;
import com.ai_junkie.SmartSweeper.printing.NodeVisitor;
import com.ai_junkie.SmartSweeper.printing.PrinterVisitor;

public class NeuralNet implements AINode {

	NeuronLayer[] neuronLayers;
	ArrayList<NeuronConnection> allCons = new ArrayList<NeuronConnection>();

//	public static double learningRate = 0.3; // eta
//	public static double momentum = 0.9; // alpha
//	public static double learningRate = 0.15; // eta
//	public static double momentum = 0.5; // alpha
	//public static double learningRate = 1; // eta
	//public static double momentum = 0.0; // alpha
	
	public NeuralNet(int[] top) {
		// Setup the layers
		neuronLayers = new NeuronLayer[top.length];
		
		for(int i = 0; i < top.length; i++) {
			neuronLayers[i] = new NeuronLayer(top[i]);
		}
		// Setup the connections between layers
		for(int i = 0; i < neuronLayers.length - 1; i++) {
			setupConnections(neuronLayers[i], neuronLayers[i + 1]);
		}
	}
	

	public void copyWeights(NeuralNet bestBrain) {
		for(int j = neuronLayers.length - 1; j > 0; j--) {
			for(int i = 0; i < neuronLayers[j].neuronsList.size(); i++) {
				Neuron myNeuron = neuronLayers[j].neuronsList.get(i);
				Neuron brainNeuron = bestBrain.neuronLayers[j].neuronsList.get(i);
				myNeuron.setOutputValue(brainNeuron.getOutputValue());
				for(int k = 0; k < neuronLayers[j].neuronsList.get(i).getOutputs().size(); k++) {
					NeuronConnection myConnection = neuronLayers[j].neuronsList.get(i).getOutputs().get(k);
					NeuronConnection brainsConnection = bestBrain.neuronLayers[j].neuronsList.get(i).getOutputs().get(k);
					myConnection.setDelta(brainsConnection.getDelta());
					myConnection.setValue(brainsConnection.getValue());
					myConnection.setWeight(brainsConnection.getWeight());
					myConnection.setGradient(brainsConnection.getGradient());
				}
			}
		}
	}
	
	private void setupConnections(NeuronLayer left, NeuronLayer right) {
		for(int i = 0; i < left.neuronsList.size(); i++) {
			for(int j = 0; j < right.neuronsList.size(); j++) {
				NeuronConnection nc = new NeuronConnection(left.neuronsList.get(i), right.neuronsList.get(j));
				left.neuronsList.get(i).getOutputs().add(nc);
				right.neuronsList.get(j).getInputs().add(nc);
				allCons.add(nc);
			}
		}
		for(int j = 0; j < right.neuronsList.size(); j++) {
			NeuronConnection nc = new NeuronConnection(left.bias, right.neuronsList.get(j));
			left.bias.getOutputs().add(nc);
			right.neuronsList.get(j).getInputs().add(nc);
			allCons.add(nc);
		}
	}

	public void feedForward(ArrayList<Double> inputValues) {
		neuronLayers[0].feedForward(inputValues);
		for(int i = 1; i < neuronLayers.length; i++) {
			neuronLayers[i].feedForward();
		}
	}
	
	public void backPropagate(ArrayList<Double> targetValues) {
		
		neuronLayers[neuronLayers.length - 1].calcDelta(targetValues);
		for(int i = neuronLayers.length - 2; i >= 0; i--) {
			neuronLayers[i].calcDelta();
		}
		
		for(int i = neuronLayers.length - 1; i > 0; i--) {
			neuronLayers[i].calcGradients();
		}
		
//		Iterator<NeuronLayer> i = neuronLayers.descendingIterator();
//		i.next().calcDelta(targetValues);
//		while(i.hasNext()) {
//			i.next().calcDelta();
//		}
//		i = neuronLayers.descendingIterator();
//		i.next().calcGradients(targetValues);
//		while(i.hasNext()) {
//			i.next().calcGradients();
//		}
	}
	
	public ArrayList<Double> getResults() {
		return neuronLayers[neuronLayers.length - 1].getResults();
	}
	
	private void printWeights() {
		for(NeuronConnection nc: allCons) {
			System.out.println(nc.left.name + " -> " + nc.right.name + " Weight: " + nc.getWeight());
		}
	}
	
	private void printGradients() {
		for(NeuronConnection nc: allCons) {
			System.out.println(nc.left.name + " -> " + nc.right.name + " Gradient: " + nc.getGradient());
		}
	}
	
	private void printDeltas() {
		for(NeuronConnection nc: allCons) {
			System.out.println(nc.left.name + " -> " + nc.right.name + " Delta: " + nc.getDelta());
		}
	}
	
	public static void main(final String args[]) {
		int[] top = {2, 2, 1};

		NeuralNet net = new NeuralNet(top);
		PrinterVisitor pv = new PrinterVisitor(System.out);
		net.Accept(pv);
		System.out.println("This output test should match the results at: http://www.heatonresearch.com/wiki/Back_Propagation");
		
		net.neuronLayers[2].neuronsList.get(0).name = "O1";
		net.neuronLayers[2].neuronsList.get(0).getInputs().get(0).setWeight(-0.22791948943117624);
		net.neuronLayers[2].neuronsList.get(0).getInputs().get(1).setWeight(0.581714099641357);
		net.neuronLayers[2].neuronsList.get(0).getInputs().get(2).setWeight(0.7792991203673414);

		net.neuronLayers[1].neuronsList.get(0).name = "H1";
		net.neuronLayers[1].neuronsList.get(0).getInputs().get(0).setWeight(-0.06782947598673161);
		net.neuronLayers[1].neuronsList.get(0).getInputs().get(1).setWeight(0.22341077197888182);
		net.neuronLayers[1].neuronsList.get(0).getInputs().get(2).setWeight(-0.4635107399577998);
		
		net.neuronLayers[1].neuronsList.get(1).name = "H2";
		net.neuronLayers[1].neuronsList.get(1).getInputs().get(0).setWeight(0.9487814395569221);
		net.neuronLayers[1].neuronsList.get(1).getInputs().get(1).setWeight(0.461587116462548);
		net.neuronLayers[1].neuronsList.get(1).getInputs().get(2).setWeight(0.09750161997450091);
		net.neuronLayers[1].bias.name = "B2";
		
		net.neuronLayers[0].neuronsList.get(0).name = "I1";
		net.neuronLayers[0].neuronsList.get(1).name = "I2";
		net.neuronLayers[0].bias.name = "B1";
		
		net.printWeights();
		
		ArrayList<Double> inputs = new ArrayList<Double>();
		inputs.add(0.0);
		inputs.add(0.0);
		
		ArrayList<Double> outputs = new ArrayList<Double>();
		outputs.add(0.0);
		
//		net.feedForward(inputs);
//		System.out.println("Should be [" + 0.0 + "]: " + net.getResults());
//		net.backPropagate(outputs);
		
		inputs.set(0, 1.0);
		inputs.set(1, 0.0);
		outputs.set(0, 1.0);
		
		net.feedForward(inputs);
		System.out.println("Should be [" + 1.0 + "]: " + net.getResults());
		net.backPropagate(outputs);
		
		net.printWeights();
		net.printGradients();
		
		inputs.set(0, 0.0);
		inputs.set(1, 0.0);
		outputs.set(0, 0.0);
		
		net.feedForward(inputs);
		System.out.println("Should be [" + 0.0 + "]: " + net.getResults());
		net.backPropagate(outputs);
		
		net.printWeights();
		net.printGradients();
		
		
		inputs.set(0, 1.0);
		inputs.set(1, 1.0);
		outputs.set(0, 0.0);
		
		net.feedForward(inputs);
		System.out.println("Should be [" + 0.0 + "]: " + net.getResults());
		
		net.backPropagate(outputs);
		
		net.printWeights();
		//net.printDeltas();
		//net.printGradients();
		
//		for(int i = 0; i < 20000; i++) {
//		System.out.println("Loop: " + i);
//		
//		int n1 = (int)(2 * Math.random());
//		int n2 = (int)(2 * Math.random());
//		int t = n1 ^ n2;
//		
//		inputs.set(0, (double)n1);
//		inputs.set(1, (double)n2);
//		outputs.set(0, (double)t);
//		
//		//net.Accept(pv);
//		net.feedForward(inputs);
//		//net.printWeights();
//		//net.Accept(pv);
//		System.out.println("Should be [" + n1 + ", " + n2 + "] -> [" + t + "] -> [" + net.getResults().get(0) + "]" + ((Math.abs(1 - net.getResults().get(0)) / 1) * 100) + "% Error");
//		net.backPropagate(outputs);
//		//net.printWeights();
//		//net.Accept(pv);
//	
//		//System.out.println();
//	}
//	//net.printWeights();
		
	}

	public NeuronLayer[] getNeuronLayers() {
		return neuronLayers;
	}
	public void setNeuronLayers(NeuronLayer[] neuronLayers) {
		this.neuronLayers = neuronLayers;
	}

	public void Accept(NodeVisitor visitor) {
		visitor.Visit(this);
	}

}
package com.ai_junkie.SmartSweeper.nodes;

import java.util.ArrayList;
import java.util.LinkedList;

import com.ai_junkie.SmartSweeper.printing.AINode;
import com.ai_junkie.SmartSweeper.printing.NodeVisitor;

public class NeuronLayer implements AINode {
	
	public LinkedList<Neuron> neuronsList = new LinkedList<Neuron>();
	public Neuron bias = new Neuron();

	public NeuronLayer(int numNeurons) {
		for (int i = 0; i < numNeurons; i++) {
			neuronsList.add(new Neuron());
		}
	}
	
	public void feedForward(ArrayList<Double> inputValues) {
		for(int i = 0; i < neuronsList.size(); i++) {
			neuronsList.get(i).feedForward(inputValues.get(i));
		}
		bias.feedForward(1.0);
	}
	
	public void feedForward() {
		for(int i = 0; i < neuronsList.size(); i++) {
			neuronsList.get(i).feedForward();
		}
		if(bias.getOutputs().size() > 0) {
			bias.feedForward(1.0);
		}
	}
	
	public void calcDelta(ArrayList<Double> targetValues) {
		for(int i = 0; i < neuronsList.size(); i++) {
			neuronsList.get(i).calcDelta(targetValues.get(i));
		}
	}
	
	public void calcDelta() {
		for(Neuron n: neuronsList) {
			n.calcDelta();
		}
		bias.calcDelta();
	}
	
	public void calcGradients() {
		for(Neuron n: neuronsList) {
			n.calcGradients();
		}
	}
	
	public ArrayList<Double> getResults() {
		ArrayList<Double> ret = new ArrayList<Double>();
		for(int i = 0; i < neuronsList.size(); i++) {
			ret.add(neuronsList.get(i).getOutputValue());
		}
		return ret;
	}
	
	public void Accept(NodeVisitor visitor) {
		visitor.Visit(this);
	}

}
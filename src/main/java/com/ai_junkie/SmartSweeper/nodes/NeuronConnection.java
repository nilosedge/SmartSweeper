package com.ai_junkie.SmartSweeper.nodes;

import com.ai_junkie.SmartSweeper.printing.AINode;
import com.ai_junkie.SmartSweeper.printing.NodeVisitor;

public class NeuronConnection implements AINode {

	public Neuron left;
	public Neuron right;
	private double delta = 0.0;
	private double value = 0.0;
	private double weight = -1 + (2 * Math.random());
	private double gradient = 0.0;

	public NeuronConnection(Neuron left, Neuron right) {
		this.left = left;
		this.right = right;
	}
	public double getDelta() {
		return delta;
	}
	public void setDelta(double delta) {
		this.delta = delta;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getGradient() {
		return gradient;
	}
	public void setGradient(double gradient) {
		this.gradient = gradient;
	}
	public void Accept(NodeVisitor visitor) {
		visitor.Visit(this);
	}
}

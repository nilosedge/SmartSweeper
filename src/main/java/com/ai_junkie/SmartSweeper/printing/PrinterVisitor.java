package com.ai_junkie.SmartSweeper.printing;

import java.io.PrintStream;

import com.ai_junkie.SmartSweeper.CController;
import com.ai_junkie.SmartSweeper.CMinesweeper;
import com.ai_junkie.SmartSweeper.nodes.NeuralNet;
import com.ai_junkie.SmartSweeper.nodes.Neuron;
import com.ai_junkie.SmartSweeper.nodes.NeuronConnection;
import com.ai_junkie.SmartSweeper.nodes.NeuronLayer;

public class PrinterVisitor extends PrinterUtil implements NodeVisitor {

	public PrinterVisitor(PrintStream out) {
		super(out);
	}



	public void Visit(Neuron neuron) {
		indent();
		PrintLine("<Neuron ov=" + neuron.getOutputValue() + ">");
		indent();
		PrintLine("<Input NeuronConnections>");
		for(NeuronConnection nc: neuron.getInputs()) {
			nc.Accept(this);
		}
		PrintLine("</Input NeuronConnections>");
		PrintLine("<Output NeuronConnections>");
		for(NeuronConnection nc: neuron.getOutputs()) {
			nc.Accept(this);
		}
		PrintLine("</Output NeuronConnections>");
		
		unindent();
		PrintLine("</Neuron>");
		unindent();
	}

	public void Visit(NeuronLayer neuronLayer) {
		indent();
		PrintLine("<NeuronLayer neurons=" + neuronLayer.neuronsList.size() + ">");
		for(int i = 0; i < neuronLayer.neuronsList.size(); i++) {
			neuronLayer.neuronsList.get(i).Accept(this);
		}
		PrintLine("</NeuronLayer>");
		unindent();
	}

	public void Visit(CMinesweeper cMinesweeper) {
		indent();
		PrintLine("<cMinesweeper>");
		cMinesweeper.m_ItsBrain.Accept(this);
		PrintLine("</cMinesweeper>");
		unindent();
	}

	public void Visit(CController cController) {
		indent();
		PrintLine("<cController sweepers=" + cController.sweeperPositions.size() + ">");
		for(int i = 0; i < cController.sweeperPositions.size(); i++) {
			cController.sweeperPositions.get(i).Accept(this);
		}
		PrintLine("</cController>");
		unindent();
	}

	public void Visit(NeuralNet neuralNet) {
		indent();
		PrintLine("<NeuralNet layers=" + neuralNet.getNeuronLayers().length + ">");
		for(int i = 0; i < neuralNet.getNeuronLayers().length; i++) {
			neuralNet.getNeuronLayers()[i].Accept(this);
		}
		PrintLine("</NeuralNet>");
		unindent();
	}

	public void Visit(NeuronConnection nc) {
		indent();
		PrintLine("<NeuronConnection value=" + nc.getValue() + " weight=" + nc.getWeight() + " grad=" + nc.getGradient() + " delta=" + nc.getDelta() + "/>");
		unindent();
	}

}

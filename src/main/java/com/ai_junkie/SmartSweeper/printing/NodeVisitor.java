package com.ai_junkie.SmartSweeper.printing;

import com.ai_junkie.SmartSweeper.CController;
import com.ai_junkie.SmartSweeper.CMinesweeper;
import com.ai_junkie.SmartSweeper.nodes.NeuralNet;
import com.ai_junkie.SmartSweeper.nodes.Neuron;
import com.ai_junkie.SmartSweeper.nodes.NeuronConnection;
import com.ai_junkie.SmartSweeper.nodes.NeuronLayer;

public interface NodeVisitor {

	
	void Visit(Neuron sNeuron);
	void Visit(NeuronLayer sNeuronLayer);
	void Visit(NeuralNet neuralNet);
	void Visit(NeuronConnection neuronConnection);
	
	void Visit(CMinesweeper cMinesweeper);
	void Visit(CController cController);

	

}

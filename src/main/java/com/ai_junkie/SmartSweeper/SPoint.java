package com.ai_junkie.SmartSweeper;

public class SPoint {
	protected double x;
	protected double y;
	
	SPoint() { }
	
	public SPoint(double a, double b) {
		x = a;
		y = b;
	}
	
	public SPoint(SPoint p) {
		x = p.x;
		y = p.y;
	}
	
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}	
	public String toString() {
		return "SPoint: X: " + x + " Y: " + y;
	}
}

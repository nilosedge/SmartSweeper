package com.ai_junkie.SmartSweeper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.ai_junkie.SmartSweeper.nodes.NeuralNet;

public class TestStocks {


	
	
	
	
	public static void main(String[] args) throws Exception {
		
		
		
		int[] top = {6, 3, 1};
		
		NeuralNet theBrain = new NeuralNet(top);
		
		
		ArrayList<Double> inputs = new ArrayList<Double>();
		
		//   0,   1,   2,  3,    4,     5,        6
		//Date,Open,High,Low,Close,Volume,Adj Close

		BufferedReader input = new BufferedReader(new FileReader("table3.csv"));
		String newLine = input.readLine();
		String line = null;
		String[] array;
		
		String future = "";
		String today = "";
		
		ArrayList<Double> targetValues = new ArrayList<Double>();
		targetValues.add(0.0);
		
		while((newLine = input.readLine()) != null) {
			//System.out.println(line);
			
			if(line != null) {
				array = line.split(",");
				inputs.clear();
				inputs.add(Double.valueOf(array[1])); // Open
				inputs.add(Double.valueOf(array[2])); // High
				inputs.add(Double.valueOf(array[3])); // Low
				inputs.add(Double.valueOf(array[4])); // Close
				inputs.add(Double.valueOf(array[5])); // Volume
				inputs.add(Double.valueOf(array[6])); // Adj Close
				theBrain.feedForward(inputs);
				today = array[4];
				
				ArrayList<Double> output = theBrain.getResults();
				
				
				
				array = newLine.split(",");
				future = array[4];
				
				double diff = (output.get(0) - Double.valueOf(future));
				
				System.out.println("Result: Diff: " + diff * 100);
				
				targetValues.set(0, Double.valueOf(future));
				theBrain.backPropagate(targetValues);
				
			}
			line = newLine;
		}
		
		

		
		
		
		//ArrayList<Double> targetValues = new ArrayList<Double>();
		//theBrain.backPropagate(targetValues);
	}

}

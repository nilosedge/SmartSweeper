package com.ai_junkie.SmartSweeper;


public class Utils {

	public static double RandomClamped() {
		return RandDouble() - RandDouble();
	}
	
	public static double RandDouble() {
		return Math.random();
	}
	
	public static int RandInt(int x,int y) {
		return (int)Math.random() * (y-x+1) + x;
	}
	
	public static double Vec2DLength(SVector2D v) {
	   return Math.sqrt((v.getX() * v.getX()) + (v.getY() * v.getY()));
	}
	
	public static void Vec2DNormalize(SVector2D v) {
	   double vector_length = Vec2DLength(v);
	   v.setX(v.getX() / vector_length);
	   v.setY(v.getY() / vector_length);
	}
	
	public static double Vec2DDot(SVector2D v1, SVector2D v2) {
	   return (v1.getX() * v2.getX()) + (v1.getY() * v2.getY());
	}
	
	public static int Vec2DSign(SVector2D v1, SVector2D v2) {
	  if (v1.getY()*v2.getX() > v1.getX()*v2.getY()) {
	    return 1;
	  } else {
	    return -1;
	  }
	}
	
	public static double Clamp(double arg, double min, double max) {
	   if (arg < min) {
	      return min;
	   }
	   if (arg > max) {
	      return max;
	   }
	   return arg;
	}
}

package com.ai_junkie.SmartSweeper;

public class CTimer {
	long m_CurrentTime, m_LastTime, m_NextTime, m_FrameTime, m_PerfCountFreq;
	double m_TimeElapsed, m_TimeScale;
	float m_FPS;

	public CTimer() {
		m_FPS = 0;
		m_TimeElapsed = 0.0f;
		m_FrameTime = 0;
		m_LastTime = 0;
		m_PerfCountFreq = 0;

		//how many ticks per sec do we get
		QueryPerformanceFrequency(m_PerfCountFreq);

		m_TimeScale = 1.0f/m_PerfCountFreq;
	}

	public CTimer(float fps) {
		m_FPS = fps;
		m_TimeElapsed = 0.0f;
		m_LastTime = 0;
		m_PerfCountFreq = 0;

		//how many ticks per sec do we get
		QueryPerformanceFrequency(m_PerfCountFreq);
		
		m_TimeScale = 1.0f/m_PerfCountFreq;
		
		//calculate ticks per frame
		m_FrameTime = (long)(m_PerfCountFreq / m_FPS);
	}
	
	
	public void Start() {
		//get the time
		QueryPerformanceCounter(m_LastTime);

		//update time to render next frame
		m_NextTime = m_LastTime + m_FrameTime;

		return;
	}

	public boolean ReadyForNextFrame() {
		if (m_FPS == 0) {
			System.out.println("No FPS set in timer");
			return false;
		}

		QueryPerformanceCounter(m_CurrentTime);

		if (m_CurrentTime > m_NextTime) {

			m_TimeElapsed	= (m_CurrentTime - m_LastTime) * m_TimeScale;
			m_LastTime	= m_CurrentTime;

			//update time to render next frame
			m_NextTime = m_CurrentTime + m_FrameTime;

			return true;
		}

		return false;
	}
	
	public double TimeElapsed() {
		QueryPerformanceCounter(m_CurrentTime);

		m_TimeElapsed	= (m_CurrentTime - m_LastTime) * m_TimeScale;

		m_LastTime	= m_CurrentTime;

		return m_TimeElapsed;

	}
	
	public long QueryPerformanceFrequency(long time) {
		return System.currentTimeMillis();
	}

	public long QueryPerformanceCounter(long time) {
		return System.currentTimeMillis();
	}
}

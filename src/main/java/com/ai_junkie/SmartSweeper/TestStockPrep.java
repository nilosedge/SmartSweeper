package com.ai_junkie.SmartSweeper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;

public class TestStockPrep {

	public static void main(String[] args) throws Exception {
		
		BufferedReader input = new BufferedReader(new FileReader("table.csv"));
		BufferedWriter output = new BufferedWriter(new FileWriter("table2.csv"));

		
		ArrayList<LinkedHashMap<String, Double>> hash = new ArrayList<LinkedHashMap<String, Double>>();
		
		String header = input.readLine();
		String line = header;
		
		String[] headers = line.split(",");
		
		for(int i = 1; i < headers.length; i++) {
			hash.add(new LinkedHashMap<String, Double>());
		}
		
		while((line = input.readLine()) != null) {
			//System.out.println(line);
			String[] array = line.split(",");
			for(int i = 1; i < headers.length; i++) {
				hash.get(i - 1).put(array[0], Double.valueOf(array[i]));
			}
		}
		input.close();
		
		for(int i = 0; i < hash.size(); i++) {
			hash.set(i, normalizeHash(hash.get(i)));
		}
		
		output.write(header + "\n");
		for(String index: hash.get(0).keySet()) {
			output.write(index);
			for(LinkedHashMap<String, Double> map: hash) {
				output.write("," + map.get(index).doubleValue());
			}
			output.write("\n");
		}
		
		
		//output.write(hash.toString());
		
		output.close();
		

	}
	
	
	public static LinkedHashMap<String, Double> normalizeHash(LinkedHashMap<String, Double> in) {
		
		Set<String> keys = in.keySet();
		
		double max = 0;
		double min = 0;

		int count = 0;
		for(String index: keys) {
			if(count == 0) {
				max = in.get(index).doubleValue();
				min = in.get(index).doubleValue();
			}
			if(in.get(index).doubleValue() > max) {
				max = in.get(index).doubleValue();
			}
			if(in.get(index).doubleValue() < min) {
				min = in.get(index).doubleValue();
			}
			count++;
		}
		
		LinkedHashMap<String, Double> ret = new LinkedHashMap<String, Double>();
		
		for(String index: keys) {
			ret.put(index, (in.get(index).doubleValue() - min) / (max - min));
		}
		return ret;
	}
	
	

}

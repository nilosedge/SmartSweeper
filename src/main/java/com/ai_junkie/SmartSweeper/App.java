package com.ai_junkie.SmartSweeper;

import javax.swing.JFrame;

public class App extends JFrame {

	private static final long serialVersionUID = -6081498544942389663L;
	
	private CController g_pController;

	public App() {
		super("Smart Sweepers v1.0");
		setTitle("Points");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(CParams.WindowWidth, CParams.WindowHeight);
		setLocationRelativeTo(null);
		//setUndecorated(true);
		setVisible(true);
		createBufferStrategy(2);

		g_pController = new CController(this);
		g_pController.start();
		
		while(true) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			g_pController.Update();
		}

	}    
	
	public static void main( String[] args ) {
		App a = new App();
	}

}

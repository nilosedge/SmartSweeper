package com.ai_junkie.SmartSweeper;

public class Test3 {

	private static double transFunc(double x) {
		return 1/(1+Math.exp(-x));
		//return Math.tanh(x);
	}
	
	private static double transFuncDer(double x) {
		return transFunc(x) * (1 - transFunc(x));
		
		//return ((1 - Math.tanh(x)) * (1 + Math.tanh(x)));
	}
	
	public static void main(String[] args) {
		//double prev = 0.3702043582229371;
		
		double sum = -0.5313402159445314;
		double a = transFunc(sum);
		System.out.println("Sum   : " + sum);
		System.out.println("Output: " + a);
		double i = 1;
		double delta = (i - a) * transFuncDer(sum);
		System.out.println("delta: " + delta);
		
		for(double j = -1; j <= 1; j += 0.01) {
			System.out.println(transFuncDer(j));
		}
		
		
		System.out.println(delta);
		
	}

}

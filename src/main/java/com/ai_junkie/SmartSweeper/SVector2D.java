package com.ai_junkie.SmartSweeper;



public class SVector2D extends SPoint {

   public SVector2D(double a, double b) {
	   super(a, b);
   }
	   
   public SVector2D multiply(double rhs) {
	   return new SVector2D(x * rhs, y * rhs);
   }
   
   public SVector2D divide(double rhs) {
	   return new SVector2D(x / rhs, y / rhs);
   }
	   
   public SVector2D add(SVector2D rhs) {
      return new SVector2D(x + rhs.x, y + rhs.y);
   }
	   
   public SVector2D minus(SVector2D rhs) {
      return new SVector2D(x - rhs.x, y - rhs.y);
   }
}

package com.ai_junkie.SmartSweeper;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

public class DirectionFrame extends JFrame implements Runnable {

	private static final long serialVersionUID = 9079261664590295822L;

	private double inx1, iny1, inx2, iny2, inx3;
	private int w, h;
	
	public DirectionFrame(String string, int w, int h) {
		super(string);
		setTitle("Points");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.w = w;
		this.h = h;
		setSize(w, h);
		setLocationRelativeTo(null);
		//setUndecorated(true);
		setVisible(true);
		createBufferStrategy(2);
	}

	public void run() {
		while(true) {
			BufferStrategy bf = getBufferStrategy();
	    	Graphics2D g = (Graphics2D)bf.getDrawGraphics();
	    	
	    	g.setBackground(Color.WHITE);
	    	g.clearRect(0, 0, w, h);
	    	
	    	g.setColor(Color.BLUE);
	    	g.drawLine((w/2), (h/2), (int)(inx1+(w/2)), (int)(iny1+(h/2)));
	    	
	    	g.setColor(Color.RED);
	    	g.drawLine((w/2), (h/2), (int)(inx2+(w/2)), (int)(iny2+(h/2)));

	    	g.setColor(Color.GREEN);
	    	g.drawLine((w/2), (h/2), (int)(inx3+(w/2)), (int)(inx3+(h/2)));

	    	g.dispose();
	    	bf.show();
	    	try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

	public void update(SVector2D lookingAt, SVector2D closestMine, double d) {
    	inx1 = (int)(lookingAt.x * (w/4));
    	iny1 = (int)(lookingAt.y * (h/4));
    	inx2 = (int)(closestMine.x * (w/4));
    	iny2 = (int)(closestMine.y * (h/4));
    	inx3 = (int)d * (w/4);
	}

}

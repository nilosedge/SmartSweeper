package com.ai_junkie.SmartSweeper;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;

import javax.swing.JFrame;

import com.ai_junkie.SmartSweeper.printing.AINode;
import com.ai_junkie.SmartSweeper.printing.NodeVisitor;

public class CController extends Thread implements AINode {

	public ArrayList<CMinesweeper> sweeperPositions = new ArrayList<CMinesweeper>();
	private final ArrayList<SVector2D> minePositions = new ArrayList<SVector2D>();
	
	private ArrayList<SPoint> sweeperVertexBuffer = new ArrayList<SPoint>();
	private ArrayList<SPoint> mineVertexBuffer = new ArrayList<SPoint>();

	SPoint sweeperModel[] = {new SPoint(-1, -1), new SPoint(-1, 1), new SPoint(-0.5, 1), new SPoint(-0.5, -1), new SPoint(0.5, -1), new SPoint(1, -1), new SPoint(1, 1), new SPoint(0.5, 1), new SPoint(-0.5, -0.5), new SPoint(0.5, -0.5), new SPoint(-0.5, 0.5), new SPoint(-0.25, 0.5), new SPoint(-0.25, 1.75), new SPoint(0.25, 1.75), new SPoint(0.25, 0.5), new SPoint(0.5, 0.5)};
	SPoint mineModel[] = {new SPoint(-1, -1), new SPoint(-1, 1), new SPoint(1, 1), new SPoint(1, -1)};

	int cxClient = CParams.WindowWidth;
	int cyClient = CParams.WindowHeight;
	
	int m_iTicks = 0;
	private JFrame frame;
	
	public CController(JFrame frame) {
		this.frame = frame;

		//let's create the mine sweepers
		for (int i = 0; i < CParams.iNumSweepers; i++) {
			sweeperPositions.add(new CMinesweeper(i == 0));
		}

		//initialize mines in random positions within the application window
		for (int i=0; i < CParams.iNumMines; i++) {
			minePositions.add(new SVector2D(Utils.RandDouble() * cxClient, Utils.RandDouble() * cyClient));
		}

		//fill the vertex buffers
		for (int i = 0; i < sweeperModel.length; i++) {
			sweeperVertexBuffer.add(new SPoint(sweeperModel[i]));
		}

		for (int i = 0; i < mineModel.length; ++i) {
			mineVertexBuffer.add(new SPoint(mineModel[i]));
		}
		
		frame.addKeyListener(new KeyListener() {

			public void keyPressed(KeyEvent e) {
				
				switch(e.getKeyCode()) {
					case 32:
						break;
					case 37:
						// Turn Left
						sweeperPositions.get(0).m_lTrack -= 0.01;
						sweeperPositions.get(0).m_rTrack += 0.01;
						break;
					case 38:
						double avg = (sweeperPositions.get(0).m_lTrack + sweeperPositions.get(0).m_lTrack) / 2;
						// Increase speed go straight
						sweeperPositions.get(0).m_lTrack = avg + 0.01;
						sweeperPositions.get(0).m_rTrack = avg + 0.01;
						break;
					case 39:
						// Turn Right
						sweeperPositions.get(0).m_lTrack += 0.01;
						sweeperPositions.get(0).m_rTrack -= 0.01;
						break;
					case 40:
						// Slow down
						sweeperPositions.get(0).m_lTrack -= 0.01;
						sweeperPositions.get(0).m_rTrack -= 0.01;
						break;
					default:
						System.out.println(e);
				}
				Update();
			}
			public void keyTyped(KeyEvent e) {}
			public void keyReleased(KeyEvent e) {}
		});
	}
	
	public void WorldTransform(ArrayList<SPoint> VBuffer, SVector2D vPos) {
		//create the world transformation matrix
		C2DMatrix matTransform = new C2DMatrix();

		//scale
		matTransform.Scale(CParams.dMineScale, CParams.dMineScale);

		//translate
		matTransform.Translate(vPos.getX(), vPos.getY());

		//transform the ships vertices
		matTransform.TransformSPoints(VBuffer);
	}

	public boolean Update() {
		
		//System.out.println("Running Control Update");
		
		//run the sweepers through CParams::iNumTicks amount of cycles. During
		//this loop each sweepers NN is constantly updated with the appropriate
		//information from its surroundings. The output from the NN is obtained
		//and the sweeper is moved. If it encounters a mine its fitness is
		//updated appropriately,
		if(m_iTicks % 1000 == 0) {
			System.out.println("Ticks: " + m_iTicks);
		}
		if (m_iTicks++ < CParams.iNumTicks) {
			for (int i = 0; i < sweeperPositions.size(); i++) {
	
				if (!sweeperPositions.get(i).Update(minePositions)) {
					//error in processing the neural net
					System.out.println("Wrong amount of NN inputs!");
					return false;
				}
				
				int GrabHit = sweeperPositions.get(i).CheckForMine(minePositions, CParams.dMineScale);
	
				if (GrabHit >= 0) {
					sweeperPositions.get(i).incFit();
					minePositions.get(GrabHit).setX(Utils.RandDouble() * cxClient);
					minePositions.get(GrabHit).setY(Utils.RandDouble() * cyClient);
				}
	
			}
		} else {
			System.out.println("Time to update the brains");
			int bestFit = 0;
			int bestFitPos = 0;
			for(int i = 0; i < sweeperPositions.size(); i++) {
				if(sweeperPositions.get(i).getIncFit() > bestFit) {
					bestFitPos = i;
					bestFit = sweeperPositions.get(i).getIncFit();
				}
			}
			
			System.out.println("Best Fit Index: " + bestFitPos);
			System.out.println("Updating everyone brain");
			for(int i = 0; i < sweeperPositions.size(); i++) {
				if(i != bestFitPos) {
					sweeperPositions.get(i).updateBrain(sweeperPositions.get(bestFitPos));
				}
				sweeperPositions.get(i).incFit = 0;
			}
			
			// See who has the highest Fit and take their brain and give it to the rest
			m_iTicks = 0;
		}

		return true;
	}
	
	public void Render(Graphics2D g) {
		
		g.setBackground(Color.WHITE);
		
		//System.out.println("Rendering new view");
		g.clearRect(0, 0, cxClient, cyClient);
		
        g.setColor(Color.BLUE);
		
		for (int i = 0; i < minePositions.size(); ++i) {
			//grab the vertices for the mine shape
			ArrayList<SPoint> mineVB = new ArrayList<SPoint>();
			for(SPoint p: mineVertexBuffer) {
				mineVB.add(new SPoint(p));
			}

			WorldTransform(mineVB, minePositions.get(i));
			
			g.setColor(Color.RED);
			
			g.drawLine((int)mineVB.get(0).getX(), (int)mineVB.get(0).getY(), (int)mineVB.get(1).getX(), (int)mineVB.get(1).getY());
			g.drawLine((int)mineVB.get(1).getX(), (int)mineVB.get(1).getY(), (int)mineVB.get(2).getX(), (int)mineVB.get(2).getY());
			g.drawLine((int)mineVB.get(2).getX(), (int)mineVB.get(2).getY(), (int)mineVB.get(3).getX(), (int)mineVB.get(3).getY());
			g.drawLine((int)mineVB.get(3).getX(), (int)mineVB.get(3).getY(), (int)mineVB.get(0).getX(), (int)mineVB.get(0).getY());
			
		}
		
		int bestFit = 0;
		int bestFitPos = 0;
		for(int i = 0; i < sweeperPositions.size(); i++) {
			if(sweeperPositions.get(i).getIncFit() > bestFit) {
				bestFitPos = i;
				bestFit = sweeperPositions.get(i).getIncFit();
			}
		}
		
		for (int i = 0; i < sweeperPositions.size(); i++) {
			
			if(i == bestFitPos) {
				g.setColor(Color.GREEN);
			} else {
				g.setColor(Color.BLUE);
			}
			
			//grab the sweeper vertices
			ArrayList<SPoint> sweeperVB = new ArrayList<SPoint>();
			for(SPoint p: sweeperVertexBuffer) {
				sweeperVB.add(new SPoint(p));
			}

			//transform the vertex buffer
			sweeperPositions.get(i).WorldTransform(sweeperVB);

			//draw the sweeper left track

			g.drawLine((int)sweeperVB.get(0).getX(), (int)sweeperVB.get(0).getY(), (int)sweeperVB.get(1).getX(), (int)sweeperVB.get(1).getY());
			g.drawLine((int)sweeperVB.get(1).getX(), (int)sweeperVB.get(1).getY(), (int)sweeperVB.get(2).getX(), (int)sweeperVB.get(2).getY());
			g.drawLine((int)sweeperVB.get(2).getX(), (int)sweeperVB.get(2).getY(), (int)sweeperVB.get(3).getX(), (int)sweeperVB.get(3).getY());
			g.drawLine((int)sweeperVB.get(3).getX(), (int)sweeperVB.get(3).getY(), (int)sweeperVB.get(0).getX(), (int)sweeperVB.get(0).getY());
			
			g.drawLine((int)sweeperVB.get(4).getX(), (int)sweeperVB.get(4).getY(), (int)sweeperVB.get(5).getX(), (int)sweeperVB.get(5).getY());
			g.drawLine((int)sweeperVB.get(5).getX(), (int)sweeperVB.get(5).getY(), (int)sweeperVB.get(6).getX(), (int)sweeperVB.get(6).getY());
			g.drawLine((int)sweeperVB.get(6).getX(), (int)sweeperVB.get(6).getY(), (int)sweeperVB.get(7).getX(), (int)sweeperVB.get(7).getY());
			g.drawLine((int)sweeperVB.get(7).getX(), (int)sweeperVB.get(7).getY(), (int)sweeperVB.get(4).getX(), (int)sweeperVB.get(4).getY());
			
			g.drawLine((int)sweeperVB.get(8).getX(), (int)sweeperVB.get(8).getY(), (int)sweeperVB.get(9).getX(), (int)sweeperVB.get(9).getY());
			
			g.drawLine((int)sweeperVB.get(10).getX(), (int)sweeperVB.get(10).getY(), (int)sweeperVB.get(11).getX(), (int)sweeperVB.get(11).getY());
			g.drawLine((int)sweeperVB.get(11).getX(), (int)sweeperVB.get(11).getY(), (int)sweeperVB.get(12).getX(), (int)sweeperVB.get(12).getY());
			g.drawLine((int)sweeperVB.get(12).getX(), (int)sweeperVB.get(12).getY(), (int)sweeperVB.get(13).getX(), (int)sweeperVB.get(13).getY());
			g.drawLine((int)sweeperVB.get(13).getX(), (int)sweeperVB.get(13).getY(), (int)sweeperVB.get(14).getX(), (int)sweeperVB.get(14).getY());
			g.drawLine((int)sweeperVB.get(14).getX(), (int)sweeperVB.get(14).getY(), (int)sweeperVB.get(15).getX(), (int)sweeperVB.get(15).getY());
			g.drawLine((int)sweeperVB.get(15).getX(), (int)sweeperVB.get(15).getY(), (int)sweeperVB.get(10).getX(), (int)sweeperVB.get(10).getY());

		}


	}

	public void run() {
		super.run();
		while(true) {
			BufferStrategy bf = frame.getBufferStrategy();
	    	Graphics2D g = (Graphics2D)bf.getDrawGraphics();
	    	this.Render(g);
	    	g.dispose();
	    	bf.show();
//	    	try {
//				sleep(100);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
		}
	}

	public void Accept(NodeVisitor visitor) {
		visitor.Visit(this);
	}
	
}
